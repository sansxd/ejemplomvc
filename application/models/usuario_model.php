<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
  //inserta datos a la bd
  public function insertar_usuario($data)
  {
    $this->db->insert('usuario',array(
      'nombre' =>$data['nombre'],
      'apellido' =>$data['apellido'],
      'telefono'=>$data['telefono'],
      'email'=>$data['email'],
      'password'=>$data['password']
    ));
  }
  //modifica los datos de usuario
  public function actualizar_usuario($data)
  {
    $this->db->where('id_usuario',$data['id_usuario'])
    ->update('usuario',array(
      'nombre' =>$data['nombre'],
      'apellido' =>$data['apellido'],
      'telefono'=>$data['telefono'],
      'email'=>$data['email'],
      'password'=>$data['password']
    ));
  }

  //mostrar usuarios a tavez de una lista
  public function listar_usuario()
  {
    return $this->db
    ->select('id_usuario, nombre, apellido, telefono,email, password')
    ->from('usuario')
    ->get()
    ->result();
  }
//elimina los datos de usuaurio
public function eliminar_usuario($data)
{
  $this->db
  ->where('id_usuario',$data['id_usuario']);
  $this->db->delete('usuario');
  /*$this->db->delete('mytable', array('id' => $id));  // Produces: // DELETE FROM mytable  // WHERE id = $id
    otra forma de hacer delete a una tabla */
}



}

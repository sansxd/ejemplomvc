<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }
//con esta funcion se llama al index para que se vea el usuario
    public function index()
  {
    $this->load->view('usuario/index');
  }

}

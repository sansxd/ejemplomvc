<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class usuario_ajax extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('usuario_model');
    $this->request = json_decode(file_get_contents('php://input'));
  }

  public function recuperar_usuarios()
  {
    $usuario = $this->usuario_model->listar_usuario();
    echo json_encode($usuario);

  }
  public function crear_usuario()
  {
    $this->usuario_model->insertar_usuario(array(
      'nombre'=> $this->request->nombre,
      'apellido'=> $this->request->apellido,
      'telefono'=> $this->request->telefono,
      'email'=> $this->request->email,
      'password'=> $this->request->password
    ));
  }
  public function modificar_usuario()
  {
    $this->usuario_model->actualizar_usuario(array(
      'id_usuario'=> $this->request->id_usuario,
      'nombre'=> $this->request->nombre,
      'apellido'=> $this->request->apellido,
      'telefono'=> $this->request->telefono,
      'email'=> $this->request->email,
      'password'=> $this->request->password

    ));
  }

  public function eliminar_usuario()
  {
    $this->usuario_model->eliminar_usuario(array(
      'id_usuario'=> $this->request->id_usuario
    ));
  }

}

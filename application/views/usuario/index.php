<html>
<head>
	<meta charset="utf-8" />
	<title> Lista de tareas </title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/bootstrap-responsive.min.css" />

</head>
<body>
	<div class="container">
		<h1> Lista de usuarios </h1>
		<form action="javascript:void(0);" id="controlar_usuario">
			<div v-if="cargando_usuarios">
				Cargando lista...
			</div>
			<table class="table" v-if="!cargando_usuarios">
				<thead>
					<tr>
						<th> Nombre </th>
						<th> Apellido </th>
						<th> Telefono </th>
            <th> Email </th>
            <th> Contraseña </th>
						<th> Acciones </th>
					</tr>
				</thead>
				<tbody>
					<!-- Fila para modificar una tarea. -->
					<tr v-for="tar in usuarios">
						<td>
							<input type="text" v-model="tar.nombre" />
						</td>
						<td>
							<input type="text" v-model="tar.apellido" />
						</td>
						<td>
							<input type="number" v-model="tar.telefono" />
						</td>
						<td>
							<input type="text" v-model="tar.email" />
						</td>
						<td>
							<input type="text" v-model="tar.password" />
						</td>
						<td>
							<button class="btn btn-success" v-on:click="modificarUsuario(tar)"> Guardar </button>
							<button class="btn btn-danger" v-on:click="eliminarUsuario(tar)"> Eliminar </button>
						</td>
					</tr>
					<!-- Fin Fila para modificar un usuario. -->
					<!-- Fila para guardar un usuario. -->
					<tr>
						<td>
							<input type="text" v-model="nuevo_usuario.nombre" />
						</td>
						<td>
							<input type="text" v-model="nuevo_usuario.apellido" />
						</td>
						<td>
							<input type="number" v-model="nuevo_usuario.telefono" />
						</td>
						<td>
							<input type="text" v-model="nuevo_usuario.email" />
						</td>
						<td>
							<input type="text" v-model="nuevo_usuario.password" />
						</td>

						<td>
							<button class="btn btn-success" v-on:click="crearUsuario()"> Guardar </button>
						</td>
					</tr>
					<!-- Fin Fila para guardar  -->
				</tbody>
			</table>
		</form>
	</div>

	<script type="text/javascript" src="<?php echo base_url() ?>js/vue.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>js/vue-resource.min.js"></script>
	<script type="text/javascript" src="https://unpkg.com/axios/dist/axios.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>js/usuario-app.js"></script>

</body>
</html>

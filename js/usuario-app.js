var controlar_usuario = new Vue({
  el: '#controlar_usuario',
  data:{
    cargando_usuarios: true,
    nuevo_usuario:{
      nombre:'',
      apellido:'',
      telefono:'',
      email:'',
      password:''
    },
    usuarios: []
  },
  methods:{
    recuperarUsuarios: function(){
      axios.get('recuperar_usuarios')
      .then(response =>{
        this.usuarios = response.data
        this.cargando_usuarios = false;
      }).catch(function (error) {
 console.log(error);
});
    },

    //   this.cargando_usuarios = true;
    //   this.$http.get('recuperar_usuarios').then(function(respuesta){
    //     this.tareas = respuesta.body;
    //     this.cargando_usuarios = false;
    //   }, function(){
    //     alert('No se han podido recuperar los usuarios.');
    //     this.cargando_usuarios = false;
    //   });
    // },
    crearUsuario: function(){
      this.$http.post('crear_usuario', this.nuevo_usuario).then(function(){
        this.nuevo_usuario.nombre='';
        this.nuevo_usuario.apellido='';
        this.nuevo_usuario.telefono='';
        this.nuevo_usuario.email='';
        this.nuevo_usuario.password='';
        this.recuperarUsuarios();
      },function(){
        alert('No se ha podido crear el usuario.');
      });
    },
    modificarUsuario: function(p_usuario){
      this.$http.post('modificar_usuario', p_usuario).then(function(){
        this.recuperarUsuarios();
      }, function(){
        alert('No se pudo modificar el usuario.');
      });
    },

    eliminarUsuario: function(p_usuario){
			this.$http.post('eliminar_usuario', p_usuario).then(function(){
				this.recuperarUsuarios();
			}, function(){
				alert('No se pudo eliminar el usuario.');
			});
		}

  },
  created: function(){
		this.recuperarUsuarios();
}




});
